import { shallowMount } from "@vue/test-utils";
import HelloWorld from "@/components/HelloWorld.vue";

describe("HelloWorld.vue", () => {
  it("renders props.msg when passed", () => {
    const msg = "new message";
    const wrapper = shallowMount(HelloWorld, {
      propsData: { msg }
    });
    expect(wrapper.text()).toMatch(msg);
  });

  it("Shows when showMe is true", () => {
    const wrapper = shallowMount(HelloWorld, {
      propsData: { msg: "testo", showMe: true }
    });
    expect(wrapper.find(".showMe").exists()).toBe(true);
  });
});
